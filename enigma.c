#include <minix/drivers.h>
#include <minix/chardriver.h>
#include <minix/ds.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "enigma.h"

/*
 * Function prototypes for the enigma driver.
 */
static int enigma_open(devminor_t minor, int access, endpoint_t user_endpt);
static int enigma_close(devminor_t minor);
static ssize_t enigma_read(devminor_t minor, u64_t position, endpoint_t endpt,
    cp_grant_id_t grant, size_t size, int flags, cdev_id_t id);
static ssize_t enigma_write(devminor_t minor, u64_t position, endpoint_t endpt,
    cp_grant_id_t grant, size_t size, int flags, cdev_id_t id);
    
/*
 * Function prototypes for the enigma machine 
 */
char shift_by_rotor_type(char rotor[][2], direction input_direction,
                         char input);
char shift_by_rotor_position(direction input_direction, char shift,
                             char input);
char shift_by_ring_position(direction input_direction, char shift, char input);
char use_reflector(char reflector[][2], char input);
char use_rotor(char rotor[][2], int rotor_num, direction input_direction,
               char input);
char use_plugboard(char plugboard[][2], char input);
void step_rotor(char *rotor_position);
char use_enigma(char input);

/* SEF functions and variables. */
static void sef_local_startup(void);
static int sef_cb_init(int type, sef_init_info_t *info);
static int sef_cb_lu_state_save(int, int);
static int lu_state_restore(void);

/* Entry points to the enigma driver. */
static struct chardriver enigma_tab =
{
    .cdr_open	= enigma_open,
    .cdr_close	= enigma_close,
    .cdr_read	= enigma_read,
    .cdr_write	= enigma_write,
};

/** State variable to count the number of times the device has been opened.
 * Note that this is not the regular type of open counter: it never decreases.
 */
 
static char input[100] = {0};
static char output[100] = "Enigma!\n";

char positions[ROTOR_SIZE] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                              'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                              'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

char rotor_I[ROTOR_SIZE][2] = {
    {'A', 'E'}, {'B', 'K'}, {'C', 'M'}, {'D', 'F'}, {'E', 'L'}, {'F', 'G'},
    {'G', 'D'}, {'H', 'Q'}, {'I', 'V'}, {'J', 'Z'}, {'K', 'N'}, {'L', 'T'},
    {'M', 'O'}, {'N', 'W'}, {'O', 'Y'}, {'P', 'H'}, {'Q', 'X'}, {'R', 'U'},
    {'S', 'S'}, {'T', 'P'}, {'U', 'A'}, {'V', 'I'}, {'W', 'B'}, {'X', 'R'},
    {'Y', 'C'}, {'Z', 'J'}};

char rotor_II[ROTOR_SIZE][2] = {
    {'A', 'A'}, {'B', 'J'}, {'C', 'D'}, {'D', 'K'}, {'E', 'S'}, {'F', 'I'},
    {'G', 'R'}, {'H', 'U'}, {'I', 'X'}, {'J', 'B'}, {'K', 'L'}, {'L', 'H'},
    {'M', 'W'}, {'N', 'T'}, {'O', 'M'}, {'P', 'C'}, {'Q', 'Q'}, {'R', 'G'},
    {'S', 'Z'}, {'T', 'N'}, {'U', 'P'}, {'V', 'Y'}, {'W', 'F'}, {'X', 'V'},
    {'Y', 'O'}, {'Z', 'E'}};

char rotor_III[ROTOR_SIZE][2] = {
    {'A', 'B'}, {'B', 'D'}, {'C', 'F'}, {'D', 'H'}, {'E', 'J'}, {'F', 'L'},
    {'G', 'C'}, {'H', 'P'}, {'I', 'R'}, {'J', 'T'}, {'K', 'X'}, {'L', 'V'},
    {'M', 'Z'}, {'N', 'N'}, {'O', 'Y'}, {'P', 'E'}, {'Q', 'I'}, {'R', 'W'},
    {'S', 'G'}, {'T', 'A'}, {'U', 'K'}, {'V', 'M'}, {'W', 'U'}, {'X', 'S'},
    {'Y', 'Q'}, {'Z', 'O'}};

char reflector_B[ROTOR_SIZE][2] = {
    {'A', 'Y'}, {'B', 'R'}, {'C', 'U'}, {'D', 'H'}, {'E', 'Q'}, {'F', 'S'},
    {'G', 'L'}, {'H', 'D'}, {'I', 'P'}, {'J', 'X'}, {'K', 'N'}, {'L', 'G'},
    {'M', 'O'}, {'N', 'K'}, {'O', 'M'}, {'P', 'I'}, {'Q', 'E'}, {'R', 'B'},
    {'S', 'F'}, {'T', 'Z'}, {'U', 'C'}, {'V', 'W'}, {'W', 'V'}, {'X', 'J'},
    {'Y', 'A'}, {'Z', 'T'}};

char plugboard[PLGBRD_SIZE][2] = {
    {'B', 'Q'}, {'C', 'R'}, {'D', 'I'}, {'E', 'J'}, {'K', 'W'},
    {'M', 'T'}, {'O', 'S'}, {'P', 'X'}, {'U', 'Z'}, {'G', 'H'},
};

char rotor_notch[ROTORS_NUM] = {'Q', 'E', 'V'};
char rotor_position[ROTORS_NUM] = {'A', 'A', 'A'};
char ring_position[ROTORS_NUM] = {'A', 'A', 'A'};

char shift_by_rotor_type(char rotor[][2], direction input_direction,
                         char input) {
  int i = 0;
  char output = '\0';

  if (input_direction == RIGHT_TO_LEFT) {
    for (i = 0; i < ROTOR_SIZE; i++) {
      if (rotor[i][0] == input) {
        output = rotor[i][1];
        break;
      }
    }
  } else {
    for (i = 0; i < ROTOR_SIZE; i++) {
      if (rotor[i][1] == input) {
        output = rotor[i][0];
        break;
      }
    }
  }

  return output;
};

char shift_by_rotor_position(direction input_direction, char shift,
                             char input) {
  int i = 0;
  int shift_value = 0;
  char output = '\0';

  for (i = 0; i < ROTOR_SIZE; i++) {
    if (positions[i] == shift) {
      shift_value = i;
    }
  }

  if (input_direction == INSIDE) {
    for (i = 0; i < ROTOR_SIZE; i++) {
      if (positions[i] == input) {
        output = positions[(i + shift_value) % ROTOR_SIZE];
        break;
      }
    }
  } else {
    for (i = 0; i < ROTOR_SIZE; i++) {
      if (positions[i] == input) {
        output = positions[(ROTOR_SIZE - shift_value + i) % ROTOR_SIZE];
        break;
      }
    }
  }

  return output;
};

char shift_by_ring_position(direction input_direction, char shift, char input) {
  int i = 0;
  int shift_value = 0;
  char output = '\0';

  for (i = 0; i < ROTOR_SIZE; i++) {
    if (positions[i] == shift) {
      shift_value = i;
    }
  }

  if (input_direction == OUTSIDE) {
    for (i = 0; i < ROTOR_SIZE; i++) {
      if (positions[i] == input) {
        output = positions[(i + shift_value) % ROTOR_SIZE];
        break;
      }
    }
  } else {
    for (i = 0; i < ROTOR_SIZE; i++) {
      if (positions[i] == input) {
        output = positions[(ROTOR_SIZE - shift_value + i) % ROTOR_SIZE];
        break;
      }
    }
  }

  return output;
};

char use_reflector(char reflector[][2], char input) {
  int i = 0;
  char output = '\0';

  for (i = 0; i < ROTOR_SIZE; i++) {
    if (reflector[i][0] == input) {
      output = reflector[i][1];
      break;
    }
  }

  return output;
};

char use_rotor(char rotor[][2], int rotor_num, direction input_direction,
               char input) {
  int i = 0;
  char letter = '\0';
  char output = '\0';

  letter = input;
  letter = shift_by_ring_position(INSIDE, ring_position[rotor_num], letter);
  letter = shift_by_rotor_position(INSIDE, rotor_position[rotor_num], letter);
  letter = shift_by_rotor_type(rotor, input_direction, letter);
  letter = shift_by_rotor_position(OUTSIDE, rotor_position[rotor_num], letter);
  letter = shift_by_ring_position(OUTSIDE, ring_position[rotor_num], letter);
  output = letter;
  
  return output;
};

char use_plugboard(char plugboard[][2], char input) {
  int i = 0;
  char output = input;

  for (i = 0; i < PLGBRD_SIZE; i++) {
    if (plugboard[i][0] == input) {
      output = plugboard[i][1];
      break;
    }
    if (plugboard[i][1] == input) {
      output = plugboard[i][0];
      break;
    }
  }

  return output;
};

void step_rotor(char *rotor_position) {
  int i = 0;
  int j = 0;
  bool move_rotor[ROTORS_NUM] = {false, false, false};

  move_rotor[2] = true;

  if (rotor_position[2] == rotor_notch[2]) {
    move_rotor[1] = true;
  }
  if (rotor_position[1] == rotor_notch[1]) {
    move_rotor[1] = true;
    move_rotor[0] = true;
  }

  for (i = 0; i < ROTORS_NUM; i++) {
    if (move_rotor[i] == true) {
      for (j = 0; j < ROTOR_SIZE; j++) {
        if (positions[j] == rotor_position[i]) {
          rotor_position[i] = positions[(j + 1) % ROTOR_SIZE];
          break;
        }
      }
    }
  }
}

char use_enigma(char input) {

  char letter = '\0';
  char output = '\0';

  letter = input;
//  printf("Input: %c\n", letter);
  step_rotor(rotor_position);
//  printf("Rotors: %c%c%c\n", rotor_position[0], rotor_position[1], rotor_position[2]);
//  letter = use_plugboard(plugboard, letter);
//  printf("After plugboard: %c\n", letter);
  letter = use_rotor(rotor_III, 2, RIGHT_TO_LEFT, letter);
//  printf("After rotor_III: %c\n", letter);
  letter = use_rotor(rotor_II, 1, RIGHT_TO_LEFT, letter);
//  printf("After rotor_II: %c\n", letter);
  letter = use_rotor(rotor_I, 0, RIGHT_TO_LEFT, letter);
//  printf("After rotor_I: %c\n", letter);
  letter = use_reflector(reflector_B, letter);
//  printf("After reflector_B: %c\n", letter);
  letter = use_rotor(rotor_I, 0, LEFT_TO_RIGHT, letter);
//  printf("After rotor_I: %c\n", letter);
  letter = use_rotor(rotor_II, 1, LEFT_TO_RIGHT, letter);
//  printf("After rotor_II: %c\n", letter);
  letter = use_rotor(rotor_III, 2, LEFT_TO_RIGHT, letter);
//  printf("After rotor_III: %c\n", letter);
//  letter = use_plugboard(plugboard, letter);
//  printf("After plugboard: %c\n", letter);
  output = letter;
//  printf("Output: %c\n", output);

  return output;
}

static int enigma_open(devminor_t UNUSED(minor), int UNUSED(access),
    endpoint_t UNUSED(user_endpt))
{
    printf("enigma_open() - Rotors positions: %c%c%c\n", rotor_position[0], rotor_position[1], rotor_position[2]);
    return OK;
}

static int enigma_close(devminor_t UNUSED(minor))
{
    printf("enigma_close() - Rotors positions: %c%c%c\n", rotor_position[0], rotor_position[1], rotor_position[2]);
    return OK;
}

static ssize_t enigma_read(devminor_t UNUSED(minor), u64_t position,
    endpoint_t endpt, cp_grant_id_t grant, size_t size, int UNUSED(flags),
    cdev_id_t UNUSED(id))
{
    u64_t dev_size;
    char *ptr;
    int ret;
    char *buf = output;

    printf("enigma_read().\n");

    /* This is the total size of our device. */
    dev_size = (u64_t) strlen(output);

    /* Check for EOF, and possibly limit the write size. */
    if (position >= dev_size) return 0;		/* EOF */
    if (position + size > dev_size)
        size = (size_t)(dev_size - position);	/* limit size */

    /* Copy the requested part to the driver. */
    ptr = buf + (size_t)position;
    if ((ret = sys_safecopyto(endpt, grant, 0, (vir_bytes) ptr, size)) != OK)
        return ret;

    /* Return the number of bytes write. */
    return size;
}

static ssize_t enigma_write(devminor_t UNUSED(minor), u64_t position,
    endpoint_t endpt, cp_grant_id_t grant, size_t size, int UNUSED(flags),
    cdev_id_t UNUSED(id))
{
    u64_t dev_size;
    char *ptr;
    int ret;
    char *buf = input;
    int i = 0;
    char enigma_in = '\0';
    char enigma_out = '\0';
    
    printf("enigma_write().\n");

    /* This is the total size of our device. */
    dev_size = (u64_t) sizeof(input);

    /* Check for EOF, and possibly limit the write size. */
    if (position >= dev_size) return 0;		/* EOF */
    if (position + size > dev_size)
        size = (size_t)(dev_size - position);	/* limit size */
        
    /* Copy the requested part to the caller. */
    ptr = buf + (size_t)position;

    if ((ret = sys_safecopyfrom(endpt, grant, 0, (vir_bytes) ptr, size)) != OK)
     return ret;
     
     memset(output, 0, sizeof(output));
   	 for (i = 0; i < size-1; i++) {
	      output[i] = use_enigma(input[i]);
	   }
    output[size] = 10;
    output[size+1] = '\0';

    /* Return the number of bytes write. */
    return size;
}

static int sef_cb_lu_state_save(int UNUSED(state), int UNUSED(flags)) {
/* Save the state. */
    ds_publish_u32("rotor_0", (u32_t)rotor_position[0], DSF_OVERWRITE);
    ds_publish_u32("rotor_1", (u32_t)rotor_position[1], DSF_OVERWRITE);
    ds_publish_u32("rotor_2", (u32_t)rotor_position[2], DSF_OVERWRITE);
    ds_publish_u32("ring_0", (u32_t)ring_position[0], DSF_OVERWRITE);
    ds_publish_u32("ring_1", (u32_t)ring_position[1], DSF_OVERWRITE);
    ds_publish_u32("ring_2", (u32_t)ring_position[2], DSF_OVERWRITE);
    ds_publish_mem("input", input, sizeof(input), DSF_OVERWRITE);
    ds_publish_mem("output", output, sizeof(input), DSF_OVERWRITE);
    
    return OK;
}

static int lu_state_restore() {
    /* Restore the state. */
    u32_t rotor_0 = 0;
    u32_t rotor_1 = 0;
    u32_t rotor_2 = 0;
    u32_t ring_0 = 0;
    u32_t ring_1 = 0;
    u32_t ring_2 = 0;
    size_t input_size = sizeof(input);
    size_t output_size = sizeof(output);

    ds_retrieve_u32("rotor_0", &rotor_0);
    ds_retrieve_u32("rotor_1", &rotor_1);
    ds_retrieve_u32("rotor_2", &rotor_2);
    ds_retrieve_u32("ring_0", &ring_0);
    ds_retrieve_u32("ring_1", &ring_1);
    ds_retrieve_u32("ring_2", &ring_2);
    ds_retrieve_mem("input", input, &input_size);
    ds_retrieve_mem("output", output, &output_size);

    ds_delete_u32("rotor_0");
    ds_delete_u32("rotor_1");
    ds_delete_u32("rotor_2");
    ds_delete_u32("ring_0");
    ds_delete_u32("ring_1");
    ds_delete_u32("ring_2");
    ds_delete_mem("input");
    ds_delete_mem("output");

    rotor_position[0] = (char)rotor_0;
    rotor_position[1] = (char)rotor_1;
    rotor_position[2] = (char)rotor_2;
    ring_position[0] = (char)ring_0;
    ring_position[1] = (char)ring_1;
    ring_position[2] = (char)ring_2;

    return OK;
}

static void sef_local_startup()
{
    /*
     * Register init callbacks. Use the same function for all event types
     */
    sef_setcb_init_fresh(sef_cb_init);
    sef_setcb_init_lu(sef_cb_init);
    sef_setcb_init_restart(sef_cb_init);

    /*
     * Register live update callbacks.
     */
    sef_setcb_lu_state_save(sef_cb_lu_state_save);

    /* Let SEF perform startup. */
    sef_startup();
}

static int sef_cb_init(int type, sef_init_info_t *UNUSED(info))
{
/* Initialize the enigma driver. */
    int do_announce_driver = TRUE;

    rotor_position[0] = 'A';
    rotor_position[1] = 'A';
    rotor_position[2] = 'A';
    ring_position[0] = 'A';
    ring_position[1] = 'A';
    ring_position[2] = 'A';
    
    switch(type) {
        case SEF_INIT_FRESH:
            printf("%s", HELLO_MESSAGE);
            printf("Rotors positions: %c%c%c\n", rotor_position[0], rotor_position[1], rotor_position[2]);
        break;

        case SEF_INIT_LU:
            /* Restore the state. */
            lu_state_restore();
            do_announce_driver = FALSE;

            printf("%sNew version of enigma!\n", HELLO_MESSAGE);
            printf("Rotors positions: %c%c%c\n", rotor_position[0], rotor_position[1], rotor_position[2]);
        break;

        case SEF_INIT_RESTART:
            printf("%sEnigma restarted!\n", HELLO_MESSAGE);
            printf("Rotors positions: %c%c%c\n", rotor_position[0], rotor_position[1], rotor_position[2]);
        break;
    }

    /* Announce we are up when necessary. */
    if (do_announce_driver) {
        chardriver_announce();
    }

    /* Initialization completed successfully. */
    return OK;
}

int main(void)
{
    /*
     * Perform initialization.
     */
    sef_local_startup();

    /*
     * Run the main loop.
     */
    chardriver_task(&enigma_tab);
    return OK;
}

