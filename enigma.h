#ifndef __ENIGMA_H
#define __ENIGMA_H

/** The Hello message. */
#define HELLO_MESSAGE "Enigma says ILBDA!\n"
#define ROTOR_SIZE (26)
#define ROTORS_NUM (3)
#define PLGBRD_SIZE (10)

typedef enum {
	RIGHT_TO_LEFT = 0,
	LEFT_TO_RIGHT,
	INSIDE,
	OUTSIDE
} direction;

#endif /* __ENIGMA_H */
